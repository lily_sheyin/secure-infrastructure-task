# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Vault manages the following

- Database: user/password
- Message Queues: passwd
- All systems: ssh keys
- Webservers: ssh, java keystores

AWS Roles
Build: God mode
Password: Access to Vault for passwords
Web: Access to Vault for certs

Jenkins fires up Ansible server, installs Ansible and runs it. Jenkins also builds servers: HaProxy, Webservers and DB. Ansible provisions them. It’s like Jenkins makes the shell, Ansible specialises them with software. Ansible need to access the vault. This is done by Ansible or Jenkins grabbing the key from KMS to access S3 bucket Vault secrets which are able to unseal the Vault. The vault has the SSL cert which is downloaded by Ansible and seals the Vault. The SSL cert secures the HaProxy and hence the webservers. Ansible provisions by accessing Vault to set up the password for the database.
TLDR: Use Jenkins to install SSL cert in HaProxy and create User/Passwd for DB.

What needs building first?
Jenkins and Ansible server already exists in Ireland. Ansible needs a new directory to avoid overlaps. JK needs a new SG so only the office can access it (office IP). God Role for jenkins server. Ansible needs KMS ability.
SSL Certs HaProxy is for secure http, https which is port 443.
PHP page must use index.php as default page.
Jenkins will clone the PHP repo which is deployed to the web servers. In the config.php, the password must change when our application runs. Prior to this mySQL must be setup with a username and password. Ideally make the config.php into an Ansible template (.j2 file).
